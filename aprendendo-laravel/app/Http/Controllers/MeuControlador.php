<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class MeuControlador extends Controller
{
    public function  produtos(){
        echo("Lista produtos");
        echo("<ol>");
        echo("<li>Caneta</li>");
        echo("</ol>");
    }
    public function getNome(){
        return "Ana Laura";
    }
    public function getIdade(){
        return "18";
    }
    public function multiplicar($n1, $n2){
        return $n1*$n2;
    }
}
