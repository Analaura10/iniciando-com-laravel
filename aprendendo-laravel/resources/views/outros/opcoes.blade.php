@extends('layouts.principal')

@section('conteudo')
<div>
    <ul class="options">
        <li><a class="warning selected"  href="{{route('opcoes', 1)}}">warning</a></li>
        <li><a class="info"     href="{{route('opcoes', 2)}}">info</a></li>
        <li><a class="sucess"   href="{{route('opcoes', 3)}}">sucess</a></li>
        <li><a class="error"    href="{{route('opcoes', 4)}}">error</a></li>
    </ul>
</div>

@if(isset($opcao))
    @switch($opcao)
        @case(1)
            <x-alerta titulo='Erro fatal' tipo='warning'>
                <p>
                    <strong>
                        Erro inesperado
                    </strong>
                </p>
                <p>
                    Ocorreu um erro inesperado
                </p>
            </x-alerta>
            @break
        @case(2)
            <x-alerta titulo='Erro fatal' tipo='info'>
                <p>
                    <strong>
                        Erro inesperado
                    </strong>
                </p>
                <p>
                    Ocorreu um erro inesperado
                </p>
            </x-alerta>
            @break
        @case(3)
            <x-alerta titulo='Erro fatal' tipo='sucess'>
                <p>
                    <strong>
                        Erro inesperado
                    </strong>
                </p>
                <p>
                    Ocorreu um erro inesperado
                </p>
            </x-alerta>        
            @break 
        @case(4) 
            <x-alerta titulo='Erro fatal' tipo='error'>
                <p>
                    <strong>
                        Erro inesperado
                    </strong>
                </p>
                <p>
                    Ocorreu um erro inesperado
                </p>
            </x-alerta>
            @break 
        @default
            
    @endswitch
@endif
@endsection
    