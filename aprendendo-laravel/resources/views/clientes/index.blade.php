@extends('layouts.principal')
@section('conteudo')

<h3>{{$titulo}}</h3>
<a href="{{route('clientes.create')}}">Criar cliente</a>

@if(count($clientes)>0)
<ul>
    @foreach ($clientes as $c)
        <li>
            {{ $c['id'] }} |  {{ $c['nome'] }} 
            <a href="{{route('clientes.edit', $c['id'])}}">Editar</a>
            <a href="{{route('clientes.show', $c['id'])}}">Mostrar mais</a>
            <form method="POST" action="{{ route('clientes.destroy', $c['id']) }}">
                <!--input hidden que envia o token-->
                @csrf
                @method('DELETE')
                <input type="submit" value="Excluir">
            </form>
        </li>  
    @endforeach
</ul>
<hr>

@foreach ($clientes as $c)
    <p>
        {{  $c['nome']}} |
        @if($loop->first)
            (primeiro) |
        @endif
        @if($loop->last)
            (primeiro) |
        @endif
        {{$loop->index}} |  {{$loop->iteration}} | {{$loop->count}}
    </p>
@endforeach

@else
    <h4>Não existem clientes cadastrados </h4>
@endif

@endsection