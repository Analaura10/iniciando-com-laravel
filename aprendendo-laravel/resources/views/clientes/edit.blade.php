<h3>Editar cliente</h3>

<form method="POST" action="{{ route('clientes.update', $cliente['id']) }}">
    <!--input hidden que envia o token-->
    @csrf
    @method('PUT')
    <input type="text" name="nome" value="{{ $cliente['nome'] }}">
    <input type="submit" value="Salvar">
</form>