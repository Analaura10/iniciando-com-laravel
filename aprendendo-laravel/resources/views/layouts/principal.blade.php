<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="{{asset('css/principal.css') }}">
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css" integrity="sha384-HSMxcRTRxnN+Bdg0JdbxYKrThecOKuH5zCYotlSAcp1+c8xmyTe9GYg1l9a69psu" crossorigin="anonymous">
    <!-- Optional theme -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap-theme.min.css" integrity="sha384-6pzBo3FDv/PJ8r2KRkGHifhEocL+1X2rVCTTkUfGk7/0pbek5mMa1upzvWbrUbOZ" crossorigin="anonymous">
    <!-- Latest compiled and minified JavaScript -->
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js" integrity="sha384-aJ21OjlMXNL5UyIl/XNwTMqvzeRMZH2w8c5cRVpzpU8Y5bApTppSuUkhZXN0VxHd" crossorigin="anonymous"></script>
    <title>Views</title>
</head>

<body>
    <div class="row">
        <div class="col1">
            <div class="menu">
                <ul>
                    <li>
                        <a class="{{request()->routeIs('clientes.index') ? 'active' : '' }}"
                         href="{{route('clientes.index')}}">Clientes</a>
                    </li>
                    <li>
                        <a class="{{request()->routeIs('produtos') ? 'active' : '' }}"
                         href="{{route('produtos')}}">Produtos</a>                
                    </li>
                    <li>
                        <a class="{{request()->routeIs('departamentos') ? 'active' : '' }}"
                         href="{{route('departamentos')}}">Departamentos</a>
                    </li>
                    <li>
                        <a class="{{request()->routeIs('opcoes') ? 'active' : '' }}"
                         href="{{route('opcoes')}}">Opções</a>
                    </li>
                </ul>
            </div>
        </div>    
        <div class="col2">
            @yield('conteudo')
        </div>
    </div>
</body>

</html>