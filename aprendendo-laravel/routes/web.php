<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Http\Request;
//rota index, get, post
Route::get('/', function () {
    return view('welcome');
});

Route::get('/teste',function(){
    echo "Ola";
});

Route::get('opcoes/{opcao?}', function($opcao=null){
    return view('outros.opcoes', compact(['opcao']));
})->name('opcoes');

Route::get('produtos', function(){
    return view('outros.produtos');
})->name('produtos');

Route::get('departamentos', function(){
    return view('outros.departamentos');
})->name('departamentos');

Route::get('bootstrap', function(){
    return view('outros.exemplo');
})->name('exemplo');


Route::get('nome', 'App\Http\Controllers\MeuControlador@getNome');
Route::get('idade', 'App\Http\Controllers\MeuControlador@getIdade');
Route::get('multiplicar/{n1}/{n2}', 'App\Http\Controllers\MeuControlador@multiplicar');

Route::resource('clientes', 'ClienteControlador');


;

/*
Route::get('/ola/{nome}/{sobrenome}',function($nome, $sobrenome){
    echo "Ola, seja bem vindo(a)  $nome $sobrenome!";
});


Route::get('/seunome/{nome?}',function($nome=null){
    if(isset($nome))
        echo "Ola, seja bem vindo(a)  $nome!";
    else
        echo "você não digitou nenhum nome!";
});


Route::get('/rotacomregras/{nome}/{n}',function($nome, $n){
    for($i=0;$i<$n;$i++)
        echo " $i  - Ola, seja bem vindo(a)  $nome! <br>";
})  ->where('nome', '[A-Za-z]+')->where('n', '[0-9]+');


Route::prefix('/aplicacao')->group(function(){
    Route::get('/', function () {
        return view("app");
    })->name("app");
    Route::get('/user', function () {
        return view("user");
    })->name("app_user");
    Route::get('/profile', function () {
        return view("profile");
    })->name("app_profile");
});

Route::get('/produtos', function() {
    echo("Lista produtos");
    echo("<ol>");
    echo("<li>Caneta</li>");
    echo("</ol>");
})->name("meusprodutos");

Route::redirect("todososprodutos1", "produtos", 301);


Route::get('todosprodutos2', function() {
    return redirect()->route('meusprodutos');
});


/////////////////////////

Route::post('/requisicoes', function(Request $request) {
    return 'Hello POST';
});


Route::delete('/requisicoes', function(Request $request) {
    return 'Hello DELETE';
});

Route::put('/requisicoes', function(Request $request) {
    return 'Hello PUT';
});

Route::patch('/requisicoes', function(Request $request) {
    return 'Hello PATCH';
});

*/
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
